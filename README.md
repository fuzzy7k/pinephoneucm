## Why?
These files are intended to reduce or eliminate the echo heard by the 2nd party, which can be disorienting during a conversation.

After looking at the audio schematic for the A64 I noticed that audio is plumbed just about every way you could dream up. I dug into the A64 user manual to see if there was a register that could be modified to change things up. There are a bunch, and they are already exposed via ALSA. So, the idea is to route audio through the SoC in order to use software echo cancellation. 

## Use case
These files are for the original PinePhone only.

**voice.conf**: No echo cancellation is performed. Audio is routed directly from the microphone to the modem and from the modem to the speaker.
Only the earpiece is used, as the internal speaker is right next to the microphone and introduces a severe echo.

**pa.setup**: A script to prepare userspace for audio routed through the SOC. It splits stereo in/out into mono in/out, sets up echo cancellation, and creates loopbacks from Mic > Modem, Modem > Speaker. 

**socvoice.conf**: Audio is routed through the SOC during a Voice Call. It triggers the pa.setup script.

**jamvoice.conf**: Also triggers pa.setup. Audio is only routed through the SOC when selecting Speaker Phone.
Otherwise, audio is routed as it is in voice.conf. 
Intended for testing to provide a safe, hardware only default in case of a userspace malfunction.

## Prerequisite
A prerequisite for all of the files except voice.conf is to set up the ALSA capture card to capture at 8K, as that is what comes from the firmware.

/etc/alsa/conf.d/10-samplerate.conf:
```
       @args.RATE {
                type integer
                default 8000
        }
```
/etc/pulse/daemon.conf.d/90-pinephone.conf:
```
avoid-resampling = yes
default-sample-rate = 8000
alternate-sample-rate = 8000
```

Pipewire handles ALSA differently,

/usr/share/wireplumber/main.lua.d/50-alsa-config.lua:
```
      ["audio.format"]           = "S16LE",
      ["audio.rate"]             = 8000,
```

## How to use these
These files go in the ALSA UCM directory for the PinPhone. `/usr/share/alsa/ucm2/Pine64/PinePhone/` on Debian based distro's.
Git will not permit cloning into a populated directory, so the following procedure may be used to add these files there.
```
sudo -s
cd /usr/share/alsa/ucm2/Pine64/PinePhone/
git init
git remote add origin https://gitlab.com/fuzzy7k/pinephoneucm
git fetch origin
git checkout origin/master
```
At this point, PulseAudio will use the *socvoice.conf* after a reboot. Or, to try it out now:

```
exit
alsaucm -c PinePhone reload; systemctl --user restart pulseaudio
```


## A note on volume levels

The "Earpiece" and the "Internal Speaker" are not matched. Meaning that the maximum volume level is not the same for a given DAC output level. If the DAC is tuned to produce maximum output from the Speaker, without overdriving it, the Earpiece will not be able to reach its maximum level. Alternatively, If the Earpiece is tuned to reach it's maximum output, overdriving the Internal Speaker is possible at a moderate volume level. 

Complicating matters, device to device variance can be quite severe.

If you feel that the voice call volume is too low, this " tuning" (for lack of a better word) may be done by ear in order to find a reasonable value for the device you have. 

This is easiest with a remote ssh login, running alsamixer, and a GUI program on the phone that can play a full volume sound file. For my testing, I just used the speaker test built into the sound settings panel. Then, do the following:

1. Ensure that the system volume is maximized.
2. Mute the Line Out (Internal Speaker)
3. Unmute the Earpiece and set the volume to 100%
4. Play the sound sample while adjusting the DAC output level.

While there, it is not a bad idea to perform the same for the Internal Speaker. One PinePhone I tested actually needed a lower than 0db value, which is 160 in hardware. 

Or, there are two branches dac-medium and dac-loud that you may try. They are intended to be merged depending on the target device.

Ideally, there could be an app, or script, that produces an output waveform while monitoring the microphone for distortion in order to achieve an optimal value. Which, could then be run as part of a "first run" setup. 

